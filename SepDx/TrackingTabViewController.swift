//
//  TrackingTabViewController.swift
//  SepDx
//
//  Created by Devang Jadav on 8/20/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit

class TrackingTabViewController: UITabBarController {
    
    
    deinit{
        print("TrackingTabViewController Deinitialized.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tabBar.items?[0].title = "Number 0"
        tabBar.items?[1].title = "Number 1"

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
