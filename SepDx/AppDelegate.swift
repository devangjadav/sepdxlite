//
//  AppDelegate.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 6/28/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let settings = UIUserNotificationSettings(types: [.alert], categories: nil)
        application.registerUserNotificationSettings(settings)
        
        let notificationFacade = NotificationFacade()
        notificationFacade.publishNotification("Nguyen Le")
        //        notificationFacade.removeNotification("Nguyen Le")
        
        let prefs = UserDefaults.standard
        
        if let userID = prefs.string(forKey: "userID")
        {
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC")
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }

        
        return true
    }

    private func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
       // UINavigationBar.appearance().titleTextAttributes = [
         //   NSFontAttributeName: UIFont(name: "Lato-Light", size: 10)!
        //]
    
       // let attrs = [
         //   NSForegroundColorAttributeName: UIColor.white,
           // NSFontAttributeName: UIFont(name: "Lato-Light", size: 8)!
        //]
        
     //   UINavigationBar.appearance().titleTextAttributes = attrs
        
        
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().barTintColor = UIColor(colorLiteralRed: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        UITabBar.appearance().tintColor = UIColor.white
        
        
        return true
    }
    
 
//    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
//    {
////        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [UIUserNotificationType.alert], categories: nil))
//        print("Calling the delegate")
//        let settings = UIUserNotificationSettings(types: [.alert], categories: nil)
//        application.registerUserNotificationSettings(settings)
//        
//        
//        //        let notificationFacade = NotificationFacade()
//        //        notificationFacade.publishNotification("Nguyen Le")
//        //        notificationFacade.removeNotification("Nguyen Le")
//        
////        UIUserNotificationType; types =  UIUserNotificationTypeAlert;
//        
//        let prefs = UserDefaults.standard
//        
//        if let userID = prefs.string(forKey: "userID")
//        {
//           
//        }
//        else
//        {
////            let storyboard = UIStoryboard(name: "Login", bundle: nil)
////            let vc = storyboard.instantiateViewControllerWithIdentifier("LoginVC") 
////            self.window?.rootViewController = vc
////            self.window?.makeKeyAndVisible()
//        }
//
//        
//        return true
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification)
    {
        var patientName:String
        var reminder:String
        var userInfo = notification.userInfo
        if userInfo != nil
        {
            patientName = userInfo!["Patient Name"] as! String
            print(patientName)
            
            reminder = userInfo!["Reminder"] as! String
            print(reminder)
        }
        else
        {
            return
        }
        
        if (application.applicationState == UIApplicationState.active)
        {
            notificationAlertAppActive(patientName: patientName, reminder: reminder)
            
        }
        else if (application.applicationState == UIApplicationState.background)
        {
            //Do Nothing here
            print("Application is in the background")
        }
        else if (application.applicationState == UIApplicationState.inactive)
        {
            notifcationAlertAppInactive(patientName: patientName, reminder: reminder)
        }
    }
    
    func notifcationAlertAppInactive(patientName:String, reminder:String)
    {
        //TODO: open up view controller to patient window
    }
    
    
    
    func notificationAlertAppActive(patientName:String, reminder:String)
    {
        let alertController = UIAlertController(title: "Patient Reminder", message: "Patient: " + patientName + " needs to be reviewed", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
        }
        
        alertController.addAction(OKAction)
        
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
        
    }

    
}

