//
//  Screen18ViewController.swift
//  SepDx
//
//  Created by Devang Jadav on 10/15/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit
import Foundation

class Screen18ViewController: UIViewController {
    
    var patientName:String = ""
    var patientId:String = ""
    let patientDAO = PatientDAO.init()
    
    @IBOutlet weak var condIV: UISegmentedControl!
    @IBOutlet weak var condIvFluid: UISegmentedControl!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var chkPuledema: CheckBox!
    @IBOutlet weak var chkOther: CheckBox!
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var lblPulEdema: UILabel!
    
    deinit{
        print("Screen18ViewController Deinitialized.")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        //segment font color update
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (condIV.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condIV.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        (condIvFluid.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condIvFluid.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        
        
        lblReason.isHidden = true
        chkPuledema.isHidden = true
        chkOther.isHidden = true
        lblOther.isHidden = true
        lblPulEdema.isHidden = true
    }

    
    @IBAction func conIvFluidValueChanged(_ sender: UISegmentedControl) {
        if (condIvFluid.selectedSegmentIndex == 1)
        {
            lblReason.isHidden = false
            chkPuledema.isHidden = false
            chkOther.isHidden = false
            lblOther.isHidden = false
            lblPulEdema.isHidden = false
        } else {
            lblReason.isHidden = true
            chkPuledema.isHidden = true
            chkOther.isHidden = true
            lblOther.isHidden = true
            lblPulEdema.isHidden = true
        }
    }
    
    
    @IBAction func nextButtonTouchedUp(_ sender: UIButton) {
        //if "yes" jump to screen#13
        if (condIvFluid.selectedSegmentIndex == 0)
        {
            self.performSegue(withIdentifier: "Screen19", sender: Any?.self)
            print("secreen 19")
        } else {
            //go to next screen#20
            self.performSegue(withIdentifier: "Screen20", sender: Any?.self)
            print("secreen 20")
        }
        
    }
    
    
    
}


