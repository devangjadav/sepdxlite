//
//  ThreeHrScrTwentyOne.swift
//  SepDx
//
//  Created by Nguyen Le on 10/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class ThreeHrScrTwentyOne: UIViewController
{
    @IBOutlet weak var nextBtn: UIButton!
    
    override func viewDidLoad()
    {
        //nextBtn.layer.cornerRadius = 5
        //nextBtn.layer.borderWidth = 2
        //nextBtn.layer.borderColor = UIColor.black.cgColor
    }
    
    @IBAction func nextBtnSelected(_ sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeNav")
        self.present(vc, animated: true, completion: nil)

    }
}
