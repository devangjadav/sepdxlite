//
//  ConfirmationViewController.swift
//  SepDx
//
//  Created by Nidhi Narang on 12/31/00.
//  Copyright © 2000 MAN. All rights reserved.
//

import UIKit


class ConfirmationViewController: UIViewController, UITextFieldDelegate {
    
    var userObject: Int = 0
    
    var patientName:String = ""
    var patientId:String = ""
    let patientDAO = PatientDAO.init()
    
    deinit{
        print("ConfirmationViewController Deinitialized.")
    }
    
    @IBAction func confirmationClicked(_ sender: AnyObject) {
        
        if(confirmationSwitch.isOn) {
            doneButton.isHidden = false
            saveConfirmation(confirmation: true)
            //store the confirmation
        } else {
            doneButton.isHidden = true
            saveConfirmation(confirmation: false)
        }
        
    }
    @IBOutlet weak var confirmationSwitch: UISwitch!
    @IBOutlet weak var doneButton: UIImageView!
    override func viewWillAppear(_ animated: Bool) {
        
        
        super.viewWillAppear(animated)
        //self.tabBarController?.navigationItem.title = "My Title"
        // self.tabBarController?.navigationItem.leftBarButtonItem = settingsButton //This is the IBOutlet variable that you previously added
        loadConfirmation()
        //confirmationSwitch.isOn = false
        //doneButton.isHidden = true
      
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.title = "Tracking Patients"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userObject)
        // Do any additional setup after loading the view, typically from a nib.
        //InfectionInfoLabel.numberOfLines = 0
        
   
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ConfirmationViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func loadConfirmation() {
        let vitals = patientDAO.loadVitals(self.patientId)
        if(vitals.sepsisConfirmationDTM != NSDate.distantPast) {
            self.confirmationSwitch.isOn = true
            doneButton.isHidden = false
        } else {
            self.confirmationSwitch.isOn = false
            doneButton.isHidden = true
        }
        
        
    }
    
    func saveConfirmation(confirmation: Bool) {
        let vitals = patientDAO.loadVitals(self.patientId)
        if (confirmation) {
        vitals.sepsisConfirmationDTM = Date()
        } else {
            vitals.sepsisConfirmationDTM = NSDate.distantPast
        }
        patientDAO.addVitalSignsForPatient(vitals)

    }


}
