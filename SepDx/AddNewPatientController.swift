//
//  AddNewPatientController.swift
//  SepDx
//
//  Created by Devang Jadav on 8/9/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit
import CoreData

class AddNewPatientViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var weightTxtFld: UITextField!
    @IBOutlet weak var genderTxtFld: UISegmentedControl!
    @IBOutlet weak var initialTxtFld: UITextField!
    @IBOutlet weak var ageTxtFld: UITextField!
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        initialTxtFld.becomeFirstResponder()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

        
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (genderTxtFld.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (genderTxtFld.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (genderTxtFld.subviews[2] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddNewPatientViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

    }
    
    /*    func textFieldDidBeginEditing(textField: UITextField) {
     let datePicker = UIDatePicker()
     datePicker.datePickerMode = .Date
     textField.inputView = datePicker
     datePicker.maximumDate = NSDate()
     datePicker.addTarget(self, action: #selector(AddNewPatientViewController.datePickerChanged(_:)), forControlEvents: .ValueChanged)
     }
     
     func datePickerChanged(sender:UIDatePicker)
     {
     let formatter = NSDateFormatter()
     formatter.dateStyle = .LongStyle
     dateOfbirthTxtField.text = formatter.stringFromDate(sender.date)
     }
     
     */
    
    /*@IBAction func nextButtonTapped(sender: AnyObject) {
     print("next button tapped")
     
     let vc1 = UIStoryboard(name: "Tracking", bundle: nil).instantiateViewControllerWithIdentifier("TrackingViewController")
     
     self.presentViewController(vc1, animated: true, completion: nil)
     
     }*/
    
    @IBAction func saveBtnSelected(_ sender: AnyObject) {
        
        guard let ftext = initialTxtFld.text , !ftext.isEmpty else {
            return
        }
        guard let ltext = ageTxtFld.text , !ltext.isEmpty else {
            return
        }
        guard let dtext = weightTxtFld.text , !dtext.isEmpty else {
            return
        }
        
        let patientDAO: PatientDAO = PatientDAO.init()
        //trimm leading and trailing space for validation and saving
        let trimmedInitial = initialTxtFld.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        //let trimmedLname = lastNameTxtField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        //validate if patient already exists
        var gender : String = ""
        
        print(initialTxtFld.text)
        print(ageTxtFld.text)
        print(weightTxtFld.text)
        print(genderTxtFld.selectedSegmentIndex)
        
        if (genderTxtFld.selectedSegmentIndex == 0){
            gender = "Male"
        } else if (genderTxtFld.selectedSegmentIndex == 1) {
            gender = "Female"
        } else {
            gender = "Other"
        }
        
        
        if (patientDAO.validatePatientExists(trimmedInitial, patientAge: Int16(ageTxtFld.text!)!))
        {
            //combination of firstname, lastname and date of birth does not exist, adding new patient
            patientDAO.addPatient(initial: initialTxtFld.text!, age: Int16(ageTxtFld.text!)!, weight: weightTxtFld.text!, gender: gender)
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            //combination of firstname, lastname and date of birth already exists in the system
            let alertView = UIAlertController(title: "Patient already exists", message: "Please change the initial or age and try again", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertView, animated: true, completion: nil)
        }

    }
    
    
    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {
        guard let ftext = initialTxtFld.text , !ftext.isEmpty else {
            return
        }
        guard let ltext = ageTxtFld.text , !ltext.isEmpty else {
            return
        }
        guard let dtext = weightTxtFld.text , !dtext.isEmpty else {
            return
        }
        
        let patientDAO: PatientDAO = PatientDAO.init()
        //trimm leading and trailing space for validation and saving
        let trimmedInitial = initialTxtFld.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        //let trimmedLname = lastNameTxtField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        //validate if patient already exists
        var gender : String = ""
        
        print(initialTxtFld.text)
        print(ageTxtFld.text)
        print(weightTxtFld.text)
        print(genderTxtFld.selectedSegmentIndex)
        
        if (genderTxtFld.selectedSegmentIndex == 0){
            gender = "Male"
        } else if (genderTxtFld.selectedSegmentIndex == 1) {
            gender = "Female"
        } else {
            gender = "Other"
        }
        
        
        if (patientDAO.validatePatientExists(trimmedInitial, patientAge: Int16(ageTxtFld.text!)!))
        {
            //combination of firstname, lastname and date of birth does not exist, adding new patient
            patientDAO.addPatient(initial: initialTxtFld.text!, age: Int16(ageTxtFld.text!)!, weight: weightTxtFld.text!, gender: gender)
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            //combination of firstname, lastname and date of birth already exists in the system
            let alertView = UIAlertController(title: "Patient already exists", message: "Please change the initial or age and try again", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertView, animated: true, completion: nil)
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    
    
    
}
