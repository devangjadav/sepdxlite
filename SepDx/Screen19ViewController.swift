//
//  Screen19ViewController.swift
//  SepDx
//
//  Created by Devang Jadav on 10/15/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class Screen19ViewController: UIViewController
{
    
    @IBOutlet weak var measureCVP: UISegmentedControl!
    @IBOutlet weak var piccoDevice: UISegmentedControl!
    @IBOutlet weak var legManeuver: UISegmentedControl!
    
  //  @IBOutlet weak var nextBtn: UIButton!
    
    override func viewDidLoad() {
        //nextBtn.layer.cornerRadius = 5
        //nextBtn.layer.borderWidth = 2
        //nextBtn.layer.borderColor = UIColor.black.cgColor
        
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        //segment font color update
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (measureCVP.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (measureCVP.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        (piccoDevice.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (piccoDevice.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        (legManeuver.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (legManeuver.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        
        
    }
}

