//
//  ThreeHourScreenEigthteen.swift
//  SepDx
//
//  Created by Nguyen Le on 10/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class ThreeHourScreenEigthteen: UIViewController
{
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var questionOneSelection: UISegmentedControl!
    
    @IBOutlet weak var secondQuestionText: UITextView!
    
    @IBOutlet weak var textField: UITextField!
     override func viewDidLoad()
     {
        secondQuestionText.isHidden = true
        textField.isHidden = true
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        //segment font color update
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (questionOneSelection.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (questionOneSelection.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

      
    }
    
    @IBAction func questionOneValueChange(_ sender: AnyObject)
    {
       let selectionIndex =  questionOneSelection.selectedSegmentIndex
        
        if selectionIndex == 0
        {
           secondQuestionText.isHidden = false
           textField.isHidden = false
        }
        else
        {
            secondQuestionText.isHidden = true
            textField.isHidden = true
        }
    }
    
    @IBAction func nextBtnSelected(_ sender: AnyObject)
    {
        let selectedIndex =  questionOneSelection.selectedSegmentIndex
        
        if selectedIndex == 1
        {
            performSegue(withIdentifier: "scr20Segue", sender: self)
        }
        else
        {
            performSegue(withIdentifier: "screen19Segue", sender: self)
        }
    }
    
}
