//
//  InfectionViewController.swift
//  SepDx
//
//  Created by Nidhi Narang on 9/23/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit


class InfectionViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var InfectionInfoLabel: UILabel!
    
    @IBOutlet weak var infectionValueSwitch: UISegmentedControl!
//    @IBOutlet weak var infectionValueSwitch: UISwitch!
   
    @IBAction func toggleInfectionSwitch(_ sender: UISegmentedControl) {
        
        saveInfection()
        if(infectionValueSwitch.selectedSegmentIndex == 1) {
            InfectionInfoLabel.isHidden = false
        } else {
            InfectionInfoLabel.isHidden = true
        }
        
    }
    
    
    
    var userObject: Int = 0
    
    var patientName:String = ""
    
    var patientId:String = ""
    let patientDAO = PatientDAO.init()
    
    
   
    
    deinit{
        print("InfectionViewController Deinitialized.")
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        super.viewWillAppear(animated)
        
        
        //self.tabBarController?.navigationItem.title = "My Title"
        // self.tabBarController?.navigationItem.leftBarButtonItem = settingsButton //This is the IBOutlet variable that you previously added
        loadInfection()
        if(infectionValueSwitch.selectedSegmentIndex == 1) {
            InfectionInfoLabel.isHidden = false
        } else {
            InfectionInfoLabel.isHidden = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.title = "Tracking Patients"
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(userObject)
        
        //tabbar navigation color
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
 //       self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Lato-Light", size: 12)!]
 
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
 
      //  self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Lato-Light", size: 12)!]
        
   /*     let attrs = [
            NSFontAttributeName: UIFont(name: "Lato-Regular", size: 14)!,
            NSForegroundColorAttributeName: UIColor.white
        ]
        
        UINavigationBar.appearance().titleTextAttributes = attrs
     */
        
        //segment font color update
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (infectionValueSwitch.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (infectionValueSwitch.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        
        // Do any additional setup after loading the view, typically from a nib.
        //InfectionInfoLabel.numberOfLines = 0
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(InfectionViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        print(patientName)
        loadInfection()
        if(infectionValueSwitch.selectedSegmentIndex == 1) {
            InfectionInfoLabel.isHidden = false
        } else {
            InfectionInfoLabel.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func loadInfection () {
        let vitals = patientDAO.loadVitals(self.patientId)
       
    
        
        self.infectionValueSwitch.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: vitals.infectionSwitch)
    
    }
    
    func saveInfection() {
        
        patientDAO.saveInfection(patientDAO.convIntToBoolean(passInt: infectionValueSwitch.selectedSegmentIndex), patientId: self.patientId)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "InfectionToVitalTab"
        {
                        
            //let detailViewController = tabCtrl.viewControllers!//[0] as! InfectionNavigationController
            //let vc: UITabBarController = segue.destination as! UITabBarController
           // let dvc = vc.viewControllers![0] as! InfectionNavigationController
            let detailViewController = segue.destination as! VitalSignsViewController
            detailViewController.tabBarController?.navigationItem.title = self.patientName
            detailViewController.patientName = self.patientName
                    detailViewController.patientId = self.patientId
            detailViewController.infectionSwitch = patientDAO.convIntToBoolean(passInt: self.infectionValueSwitch.selectedSegmentIndex)
            }
            
        }
}


