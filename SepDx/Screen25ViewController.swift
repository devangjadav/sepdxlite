//
//  Screen25ViewController.swift
//  SepDx
//
//  Created by Nidhi Narang on 10/4/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class Screen25ViewController: UIViewController
{
    var userObject: Int = 0
    var patientName:String = ""
    var patientId:String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        super.viewWillAppear(animated)
        //self.tabBarController?.navigationItem.title = "My Title"
        // self.tabBarController?.navigationItem.leftBarButtonItem = settingsButton //This is the IBOutlet variable that you previously added
                
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.title = "Tracking Patients"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userObject)
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Screen25ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

}
