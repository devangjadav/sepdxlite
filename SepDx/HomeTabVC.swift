//
//  HomeTabVC.swift
//  SepDx
//
//  Created by Nguyen Le on 9/24/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class HomeTabVC: UIViewController
{
    
    override func viewDidAppear(_ animated: Bool) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeNav")
        self.present(vc, animated: true, completion: nil)
    }
}

