//
//  ThreeHrScreenDone.swift
//  SepDx
//
//  Created by Nguyen Le on 9/28/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class ThreeHrSceenDone: UIViewController
{
    @IBOutlet weak var doneBtn: UIButton!
    
    @IBAction func doneBtnSelected(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeNav")
        self.present(vc, animated: true, completion: nil)
        
    }

    override func viewDidLoad() {
        
        doneBtn.layer.cornerRadius = 5
        doneBtn.layer.borderWidth = 2
        doneBtn.layer.borderColor = UIColor.black.cgColor
        
    }
}
