//
//  ThreeHourInitialVC.swift
//  SepDx
//
//  Created by Nguyen Le on 9/30/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class ThreeHourInitialVC: UIViewController
{
    
    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var answer: UISegmentedControl!
    
    override func viewDidLoad() {
        let tbvc = self.tabBarController as! BaseTabViewVC
        let patientDTO = tbvc.patinentDTO
        var calculatedVolume:String = "NA"
        var endTime:String = "NA"
        
        if let retrievedWeight = patientDTO?.weight
        {
            let weight = Double(retrievedWeight)
            calculatedVolume = String(round(weight! * 30))
        }
        
        if let retrievedMonitor = patientDTO?.startMonitoring
        {
            let calendar = Calendar.current
            let threeHourTimeEnd:Date = calendar.date(byAdding:Calendar.Component.hour, value: 3, to: retrievedMonitor)!
            let hour = calendar.component(Calendar.Component.hour, from: threeHourTimeEnd)
            let minute = calendar.component(Calendar.Component.minute, from: threeHourTimeEnd)
            if minute < 10
            {
                endTime = "\(hour):0\(minute)"
            }
            else
            {
                endTime = "\(hour):\(minute)"
            }
        }
        
        let txtFieldText = "Give intravenous Ringer's lactate or 0.9% NaCI (normal saline), 30 mL/kg (" + calculatedVolume + ") by time " + endTime
        
        textField.text = txtFieldText
        
        //segment font color update
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (answer.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (answer.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
    }
    
    
    @IBAction func nextBtnSelected(_ sender: AnyObject)
    {
        if answer.selectedSegmentIndex == 0
        {
            let alertController = UIAlertController(title: "", message: "In order to proceed, you will need to answer yes to the questioniare", preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                
            }
            
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
}
