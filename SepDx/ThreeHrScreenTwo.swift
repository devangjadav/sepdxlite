//
//  ThreeHrScreenTwo.swift
//  SepDx
//
//  Created by Nguyen Le on 9/28/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class ThreeHrScreenTwo: UIViewController
{
    
    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var answerOne: UISegmentedControl!

    @IBOutlet weak var nextBtn: UIButton!
    
    @IBOutlet weak var answerTwo: UISegmentedControl!
    
    @IBOutlet weak var answerThree: UISegmentedControl!
    
    @IBOutlet weak var answerFour: UISegmentedControl!
    @IBAction func nextBtnSelected(_ sender: AnyObject)
    {
        if ( answerOne.selectedSegmentIndex == 1 || answerTwo.selectedSegmentIndex == 1 || answerThree.selectedSegmentIndex == 1 || answerFour.selectedSegmentIndex == 1)
        {
            performSegue(withIdentifier: "ThreeHourEighteenSeque", sender: self)
        }
        else
        {
            performSegue(withIdentifier: "threeHrDoneSegue", sender: self)
        }
    }
    
    override func viewDidLoad()
    {
      
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        //segment font color update
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (answerOne.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (answerOne.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        (answerTwo.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (answerTwo.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        

        (answerThree.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (answerThree.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        (answerFour.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (answerFour.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        

        
        let tbvc = self.tabBarController as! BaseTabViewVC
//        let tabBarItem = tbvc.tabBar.items
//        
//        let threeHourTab = (tabBarItem?[2])! as UITabBarItem
//        threeHourTab.isEnabled = true
        
        let patientDTO = tbvc.patinentDTO
        var calculatedVolume = "NA"
        
        if let retrievedWeight = patientDTO?.weight
        {
            let weight = Double(retrievedWeight)
            calculatedVolume = String(weight! * 0.5)
        }
        
        textField.text = "Is urine output < " + calculatedVolume + " mL/Kg per hour?"
        
        
        
        
    }
}
