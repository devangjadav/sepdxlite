//
//  LoginInteractor.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/8/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation

protocol LoginBusinessLogic{
    
    func performLogin()
}

class LoginInteractor:LoginBusinessLogic{
    
    weak var output: LoginDisplayLogic!
    
    init(output:LoginDisplayLogic){
        self.output = output
    }
    
    deinit{
        print("LoginInteractor Deinitialized.")
    }
    
    func performLogin()
    {
//        if output != nil{
//            dispatch_get_main_queue().asynchronously(execute: { () -> Void in
//                self.output.displayLoginResult(result: true)
//            })
//        }
    }

}
