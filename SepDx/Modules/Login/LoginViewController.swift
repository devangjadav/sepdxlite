//
//  LoginViewController.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 7/5/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit

protocol LoginDisplayLogic:class{
    func displayLoginResult(result:Bool)
}

class LoginViewController: UIViewController,LoginDisplayLogic, UITextFieldDelegate
{
    
    var output: LoginBusinessLogic!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var login: UITextField!
    

    deinit{
        print("LoginViewController Deinitialized.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        output =  LoginInteractor(output: self)
        
        loginButton.layer.cornerRadius = 5
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        self.tabBarController?.tabBar.isHidden = true
        //self.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = true

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginPressed(sender: AnyObject)
    {
        if (login.text?.isEmpty)!
        {
            displayAlertForEmptyField()
        }
        else
        {
            let userDefaults = UserDefaults.standard
            userDefaults.set(login.text, forKey:"userID")
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HomeNav")
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func displayAlertForEmptyField()
    {
        let alertController = UIAlertController(title: "User ID", message: "The User Initial cannot be empty.", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }

    
    func displayLoginResult(result:Bool){
        
        if result
        {
            print("Loading the home controller test again")
//            let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewControllerWithIdentifier("HomeViewController")
//            self.navigationController?.setViewControllers([vc], animated: true)
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HomeNav")
            self.present(vc, animated: true, completion: nil)
        }else{
            
            let alert = UIAlertController(title: "Alert", message: "Login failed !!!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
