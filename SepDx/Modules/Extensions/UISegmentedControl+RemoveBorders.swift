//
//  UISegmentedControl+RemoveBorders.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 7/18/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit

extension UISegmentedControl {
    
    func removeBorders() {
        
        if let background = backgroundColor{
            setBackgroundImage(imageWithColor(color: background), for: .normal, barMetrics: .default)
        }else{
            setBackgroundImage(imageWithColor(color: UIColor.clear), for: .normal, barMetrics: .default)
        }
        
        if let tint = tintColor{
            setBackgroundImage(imageWithColor(color: tint), for: .selected, barMetrics: .default)
        }else{
            setBackgroundImage(imageWithColor(color: UIColor.clear), for: .selected, barMetrics: .default)
        }
        
        setDividerImage(imageWithColor(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }
    
    // create a 1x1 image with this color
    private func imageWithColor(color: UIColor) -> UIImage {
//        let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 1.0, height: 1.0))
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}
