//
//  OrganFailureViewController.swift
//  SepDx
//
//  Created by Devang Jadav on 10/1/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit
import Foundation

class OrganFailureViewController: UIViewController {
    
    var patientName:String = ""
    var patientId:String = ""
    let patientDAO = PatientDAO.init()
    
    @IBOutlet weak var condPlateletCount: UISegmentedControl!
    @IBOutlet weak var condOrganHyperfusion: UISegmentedControl!
    @IBOutlet weak var condBilirubin: UISegmentedControl!
    
    deinit{
        print("OrganFailureViewController Deinitialized.")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //let timeElapsed = patientDAO.getTimeElapsed(id: "07171C6B-952B-441F-A3C8-E30B3325A1CD")
        //print(timeElapsed)
        
        //update segment font color
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (condPlateletCount.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condPlateletCount.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        (condOrganHyperfusion.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condOrganHyperfusion.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        (condBilirubin.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condBilirubin.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        
    }
    
}

