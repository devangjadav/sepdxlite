//
//  SepsisShockTimerViewController.swift
//  SepDx
//
//  Created by Devang Jadav on 10/13/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit
import Foundation

class SepsisShockTimerViewController: UIViewController {
    
    var seconds :Int = 0
    var timer = Timer()

    var patientName:String = ""
    var patientId:String = ""
    let patientDAO = PatientDAO.init()
    
    
    
    @IBOutlet weak var countdown: UILabel!
    @IBOutlet weak var lblOtherReason: UILabel!
    @IBOutlet weak var chkOther: CheckBox!
    @IBOutlet weak var lblPulReason: UILabel!
    @IBOutlet weak var chkPulm: CheckBox!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var conIvFluid: UISegmentedControl!
    
    @IBAction func conIvFluidChange(_ sender: UISegmentedControl) {
        
        if (conIvFluid.selectedSegmentIndex == 1)
        {
            lblReason.isHidden = false
            chkOther.isHidden = false
            chkPulm.isHidden = false
            lblPulReason.isHidden = false
            lblOtherReason.isHidden = false
        } else {
            lblReason.isHidden = true
            chkOther.isHidden = true
            chkPulm.isHidden = true
            lblPulReason.isHidden = true
            lblOtherReason.isHidden = true
        }
        
    }
    
    @IBAction func onNextTouchUpInside(_ sender: UIButton) {
        
        saveDetails()
        if (conIvFluid.selectedSegmentIndex == 1)
        {
            self.performSegue(withIdentifier: "HypoPerfusionConfirm", sender: Any?.self)
        } else {
            self.performSegue(withIdentifier: "ThreeHourInitialVC", sender: Any?.self)
        }
    }
    
    deinit{
        print("SepsisShockTimerViewController Deinitialized.")
    }
    
    
    
    
    func startTimer()
    {
        if !timer.isValid
        {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TimersViewController.counter), userInfo: nil, repeats: true)
        }
    }
    func counter()
    {
        if(seconds > 0){
            let (h,m,s) = patientDAO.secondsToHoursMinutesSeconds(s: seconds)
            countdown.text = String(h)+":"+String(m)+":"+String(s)
            seconds-=1
        } else {
            countdown.text = "00:00:00"
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //let timeElapsed = patientDAO.getTimeElapsed(id: "07171C6B-952B-441F-A3C8-E30B3325A1CD")
        //print(timeElapsed)
        
        //hide second questions onload
        lblReason.isHidden = true
        chkOther.isHidden = true
        chkPulm.isHidden = true
        lblPulReason.isHidden = true
        lblOtherReason.isHidden = true
        
        
        
        //update segment font color
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (conIvFluid.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (conIvFluid.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        
        
        //get selected patient details
        let tbvc = self.tabBarController as! BaseTabViewVC
        let patientDTO = tbvc.patinentDTO
        
        print("startmonitoring")
        print(patientDTO?.createDTM)
        
        
        //6 hour countdown
        //need to replace createDTM with startMOnitoring later on
        let monitorStartTime = patientDTO?.createDTM
        _ = Date.timeIntervalSinceReferenceDate
        let timeInterval: Int = Int(Date().timeIntervalSince(monitorStartTime!))
        seconds = (21600 - timeInterval)
        
        startTimer()
        loadDetails()
    }
    
    func loadDetails() {
        let checkList6Hour = patientDAO.loadChecklist6Hour(patientId: self.patientId)
        self.conIvFluid.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: checkList6Hour.conIvFluid)
        self.chkPulm.isChecked = checkList6Hour.chkPulm
        self.chkOther.isChecked = checkList6Hour.chkOther
    }
    
    func saveDetails() {
        let patientDao = PatientDAO()
        
        
        patientDao.saveShockTimer(conIvFluid: Bool(conIvFluid.selectedSegmentIndex as NSNumber), chkPulm: chkPulm.isChecked, chkOther: chkOther.isChecked, patientId: patientId)
    }
    
}
