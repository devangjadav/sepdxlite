//
//  HyperFusion2ViewController.swift
//  SepDx
//
//  Created by Devang Jadav on 10/1/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit
import Foundation

class HypoPerfusion2ViewController: UIViewController {
    
    var patientName:String = ""
    var patientId:String = ""
    let patientDAO = PatientDAO.init()
    
    @IBOutlet weak var condUrinOutput: UISegmentedControl!
    @IBOutlet weak var condMentalStatus: UISegmentedControl!
    @IBOutlet weak var condHyperfusion: UISegmentedControl!
    
    var isUrineOutput: Bool = false
    var isMentalStatus: Bool = false
    var isHyperPerfusion: Bool = false
    
    @IBAction func nextClicked(_ sender: AnyObject) {
        if(self.condUrinOutput.selectedSegmentIndex == 1) {
            isUrineOutput = true
        }
        else {
            isUrineOutput = false
        }
        
        if(self.condMentalStatus.selectedSegmentIndex == 1) {
            isMentalStatus = true
        }
        else {
            isMentalStatus = false
        }
        
        if(self.condHyperfusion.selectedSegmentIndex == 1) {
            isHyperPerfusion = true
        }
        else {
            isHyperPerfusion = false
        }
        saveHyperPerfusion2()
        
        
    }
    deinit{
        print("HypoPerfusion2ViewController Deinitialized.")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //let timeElapsed = patientDAO.getTimeElapsed(id: "07171C6B-952B-441F-A3C8-E30B3325A1CD")
        //print(timeElapsed)
        
        //update segment font color
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (condUrinOutput.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condUrinOutput.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        (condMentalStatus.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condMentalStatus.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        (condHyperfusion.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condHyperfusion.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        loadDetails()
    }
    
    func loadDetails() {
        let checkList = patientDAO.loadChecklist3Hour(patientId)
        
        self.condUrinOutput.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: checkList.condUrineOutput)
        self.condMentalStatus.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: checkList.condMentalStatus)
        self.condHyperfusion.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: checkList.condHyperPerfusion)
    }
    
    func saveHyperPerfusion2() {
        
        patientDAO.saveHyperPerfusion2(isUrineOutput, condMentalStatus: isMentalStatus, condHyperPerfusion: isHyperPerfusion, patientId: patientId)
        
        
    }
    
}
