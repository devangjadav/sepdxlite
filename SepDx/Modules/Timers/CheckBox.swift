
//
//  Checklist.swift
//  SepDx
//
//  Created by Devang Jadav on 9/25/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit

//checkbox class comments added to test repository checkin
class CheckBox: UIButton {
    
    //images
    let checkedImage =  UIImage(named: "checkbox_selected")
    let unCheckedImage = UIImage(named: "checkbox_unselected")
    
    //set boolean
    var isChecked:Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for:UIControlState())
            } else {
                self.setImage(unCheckedImage, for:UIControlState())
                
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(CheckBox.buttonClicked(_:)), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }
    
    func buttonClicked(_ sender:UIButton){
        if (sender == self)
        {
            if isChecked == true{
                isChecked = false
            }else {
                isChecked = true
            }
        }
    }
}
