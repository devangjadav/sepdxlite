//
//  Timers.swift
//  SepDx
//
//  Created by Devang Jadav on 9/25/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit
import Foundation

class TimersViewController: UIViewController {
    
    var seconds :Int = 0
    var timer = Timer()
  //  var audioPlayer = AVAudioPlayer()
    
    
    var patientName:String = ""
    var patientId:String = ""
    let patientDAO = PatientDAO.init()
    
    @IBOutlet weak var chkBloodCultures: CheckBox!
    @IBOutlet weak var chkAdminAnti: CheckBox!
    @IBOutlet weak var chkBloodLactate: CheckBox!
    @IBOutlet weak var countDown: UILabel!
    
    deinit{
        print("Timers Deinitialized.")
    }
    
    
    @IBAction func nextBtnClicked(_ sender: AnyObject) {
        
        print(chkBloodCultures.isChecked)
        print(chkAdminAnti.isChecked)
        print(chkBloodLactate.isChecked)
        saveChecklist3Hour()
        
       // performSegue(withIdentifier: "LinkToHypoPerfusion", sender: self)

        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //get selected patient details
        let tbvc = self.tabBarController as! BaseTabViewVC
        let patientDTO = tbvc.patinentDTO
        
        print("startmonitoring")
        //print(patientDTO?.startMonitoring)
        print(patientDTO?.createDTM)
        print(chkBloodCultures.isChecked)
        
        
        //3 hour countdown
        //need to replace createDTM with startMOnitoring later on
        let monitorStartTime = patientDTO?.createDTM
        _ = Date.timeIntervalSinceReferenceDate
        let timeInterval: Int = Int(Date().timeIntervalSince(monitorStartTime!))
        
        seconds = (10800 - timeInterval)
        loadChecklist3Hour()
        
        startTimer()
        
    }
    
    func startTimer()
    {
        if !timer.isValid
        {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TimersViewController.counter), userInfo: nil, repeats: true)
        }
    }
    func counter()
    {
        if(seconds > 0){
            let (h,m,s) = patientDAO.secondsToHoursMinutesSeconds(s: seconds)
            countDown.text = String(h)+":"+String(m)+":"+String(s)
            seconds-=1
        } else {
            countDown.text = "00:00:00"
        }
    }
    
    func loadChecklist3Hour() {
        let checkList3Hour = patientDAO.loadChecklist3Hour(self.patientId)
        self.chkAdminAnti.isChecked = checkList3Hour.administerAntibiotics
        self.chkBloodCultures.isChecked = checkList3Hour.obtainBloodCulture
        self.chkBloodLactate.isChecked = checkList3Hour.measureLactateLevel

        
    }
    
    func saveChecklist3Hour() {
        
        let patientDao = PatientDAO()
        patientDao.saveChecklist3Hour(chkAdminAnti.isChecked, measureLactateLevel: chkBloodLactate.isChecked, obtainBloodCulture: chkBloodCultures.isChecked, patientId: patientId)
        
    }
}
