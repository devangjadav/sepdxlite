//
//  HypoPerfusionViewController.swift
//  SepDx
//
//  Created by Devang Jadav on 10/1/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit
import Foundation

class HypoPerfusionViewController: UIViewController {
    
    var patientName:String = ""
    var patientId:String = ""
    let patientDAO = PatientDAO.init()
    var switchCon:Int = 0
    
    var isLactate: Bool = false
    var isArterial: Bool = false
    var isKidney: Bool = false
    var isPoorPerfusion: Bool = false
    
    
    @IBOutlet weak var conLactate: UISegmentedControl!
    @IBOutlet weak var conArterial: UISegmentedControl!
    @IBOutlet weak var condKidney: UISegmentedControl!
    @IBOutlet weak var condPoorPerfusion: UISegmentedControl!
    

    
    @IBAction func condPoorPerfusionChange(_ sender: UISegmentedControl) {
        
        if (condPoorPerfusion.selectedSegmentIndex == 1)
        {
            switchCon = switchCon + 1
            isPoorPerfusion = true
        } else {
            switchCon = switchCon - 1
            isPoorPerfusion = false
        }

    }
    @IBAction func conKidneyChange(_ sender: UISegmentedControl) {
        
        if (condKidney.selectedSegmentIndex == 1)
        {
            switchCon = switchCon + 1
            isKidney = true
        } else {
            switchCon = switchCon - 1
            isKidney = false
        }
        
    }
    @IBAction func conArterialChange(_ sender: UISegmentedControl) {
        
        if (conArterial.selectedSegmentIndex == 1)
        {
            switchCon = switchCon + 1
            isArterial = true
        } else {
            switchCon = switchCon - 1
            isArterial = false
        }

        
    }
    @IBAction func condLactateChange(_ sender: UISegmentedControl) {
        
        if (conLactate.selectedSegmentIndex == 1)
        {
            switchCon = switchCon + 1
            isLactate = true
        } else {
            switchCon = switchCon - 1
            isLactate = false
        }
        print("in segment change")
        
    }
    
    @IBAction func onClickNext(_ sender: AnyObject) {
        //if "yes" jump to screen#13
        print(switchCon)
        saveDetails()
        if (switchCon >= 2)
        {
            self.performSegue(withIdentifier: "SepsisShockTimer", sender: Any?.self)
            
            print("secreen 13")
        } else {
            //go to next screen#10
            
              self.performSegue(withIdentifier: "HypoPerfusion2", sender: Any?.self)
            print("secreen 10")
        }
        
    }
    
    deinit{
        print("HypoPerfusionViewController Deinitialized.")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //let timeElapsed = patientDAO.getTimeElapsed(id: "07171C6B-952B-441F-A3C8-E30B3325A1CD")
        //print(timeElapsed)
        
        //update segment font color
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        (conLactate.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (conLactate.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        (conArterial.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (conArterial.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        (condKidney.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condKidney.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        (condPoorPerfusion.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (condPoorPerfusion.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        loadDetails()

        
    }
    
    func loadDetails() {
        let checkList3Hour = patientDAO.loadChecklist3Hour(self.patientId)
        self.conLactate.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: checkList3Hour.condLactate)
        self.conArterial.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: checkList3Hour.condArterial)
        self.condKidney.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: checkList3Hour.condKidney)
        self.condPoorPerfusion.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: checkList3Hour.condPoorPerfusion)
        
    }
    
    func saveDetails() {
        
        let patientDao = PatientDAO()
        patientDao.saveHypoPerfusion(isLactate, condKidney: isKidney, condArterial: isArterial, condPoorPerfusion: isPoorPerfusion, patientId: patientId)
        
    }
    
    
    
    
}

