//
//  NotificationFacade.swift
//  SepDx
//
//  Created by Nguyen Le on 9/5/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class NotificationFacade
{
    func publishNotification(_ patientName: String)
    {
        let firstNotificationDate = Date(timeIntervalSinceNow: 90 * 60)
        let secondNotificationDate = Date(timeIntervalSinceNow: 150 * 60)
        let thirdNotificationDate = Date(timeIntervalSinceNow: 180 * 60)
     
        let firstNotification = constructNotification(patientName)
        firstNotification.fireDate = firstNotificationDate
        firstNotification.userInfo = ["Patient Name": patientName, "Reminder": "90"]
        UIApplication.shared.scheduleLocalNotification(firstNotification)

        let secondNofication = constructNotification(patientName)
        secondNofication.fireDate = secondNotificationDate
        secondNofication.userInfo = ["Patient Name": patientName, "Reminder": "150"]
        UIApplication.shared.scheduleLocalNotification(secondNofication)

        let thirdNotification = constructNotification(patientName)
        thirdNotification.fireDate = thirdNotificationDate
        thirdNotification.userInfo = ["Patient Name": patientName, "Reminder": "180"]
        UIApplication.shared.scheduleLocalNotification(thirdNotification)
        
//        let testDate = Date(timeIntervalSinceNow: 20)
//        let testNotification = constructNotification(patientName)
//        testNotification.fireDate = testDate
//        testNotification.userInfo = ["Patient Name": patientName, "Reminder": "90"]
//        UIApplication.shared.scheduleLocalNotification(testNotification)
        
        
        
        
    }
    
    func constructNotification(_ patientName:String) -> UILocalNotification
    {
        let notification = UILocalNotification()
        notification.alertBody = "Patient: " + patientName + " needs to be monitored"
        notification.alertAction = "Open"
        notification.category = "reminderNotification"
        notification.userInfo = ["Patient Name": patientName, "Reminder": "90"]
        return notification
    }
    
    func notificationTest(patientName:String)
    {
        let notification = UILocalNotification()
        notification.alertBody = "How are you Le"
        notification.alertAction = "Open"
        notification.category = "reminderNotification"
        notification.fireDate = NSDate(timeIntervalSinceNow: 20) as Date
        notification.userInfo = ["Patient Name": patientName, "Reminder": "90"]
        UIApplication.shared.scheduleLocalNotification(notification)
        
    }
    
    func removeNotification(patientName: String)
    {
        let app:UIApplication = UIApplication.shared
        for oneEvent in app.scheduledLocalNotifications!
        {
            let notification = oneEvent as UILocalNotification
            let userInfoCurrent = notification.userInfo! as! [String:AnyObject]
            let pName = userInfoCurrent["Patient Name"]! as! String
            if patientName == pName {
                //Cancelling local notification
                app.cancelLocalNotification(notification)
            }
        }
    }
}
