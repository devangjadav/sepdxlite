//
//  PatientsTableViewCell.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 7/12/16.
//  Copyright © 2016 MAN. All rights reserved.
//


import UIKit

class PatientListCell: UITableViewCell {
    
    @IBOutlet weak var patientName: UILabel!
    @IBOutlet weak var status: UIView!
    @IBOutlet weak var patientDob: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var countDown: UILabel!
  
    var patientCell: PatientInfo? {
        didSet {
            stylizeView()
        }
    }
    
    internal func stylizeView(){
        let dSize: CGFloat = min(status.frame.height, status.frame.width)
        status.bounds = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: dSize, height: dSize))
//        status.bounds = CGRectMake(0, 0, dSize, dSize)
        status.clipsToBounds = true
        status.layer.cornerRadius = dSize/2.0
        status.layer.borderWidth = 1.0
    }
    
    internal func stylizeView(cell: PatientListCell, patientUid: String){
    
    /*  case Incomplete = 0 //yellow, patient created, details entered, clinician not agreed with system assessment, not started monitoring
        case Active     = 1 //green - activerly monitored, ongoing 3 hour or 6 hour timer
        case Overdue    = 2 //red - 3 hour timer elapsed, checklist not completed
        case Moncomplete    = 3 //green with check, checklist completed for both 3 hour and 6 hour timer
        case postOverdue    = 4  // red with check, 6 hour timer elapsed, checklist not completed
    */
        
        if patientCell?.status == 0 {
            status.layer.borderColor = UIColor.yellow.cgColor
            status.backgroundColor = UIColor.yellow
        }else if patientCell?.status == 1 {
            status.layer.borderColor = UIColor.green.cgColor
            status.backgroundColor = UIColor.green
        }else if patientCell?.status == 2 {
            status.layer.borderColor = UIColor.red.cgColor
            status.backgroundColor = UIColor.red
        }else if patientCell?.status == 3 {
            status.layer.borderColor = UIColor.green.cgColor
            status.backgroundColor = UIColor.green
            cell.tintColor = UIColor.green
            cell.accessoryType = .checkmark
        }else if patientCell?.status == 4 {
            status.layer.borderColor = UIColor.red.cgColor
            status.backgroundColor = UIColor.red
            cell.tintColor = UIColor.red
            cell.accessoryType = .checkmark
        }
    }
    
}
