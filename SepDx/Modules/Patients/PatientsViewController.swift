//
//  PatientsViewController.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 7/5/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit
import Foundation

struct PatientInfo {
    let name : String
    let status : Int32
    let dob : String
    let id : String
    let monitorStartTime : Date
}

class PatientsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var patientList: UITableView!
    
    var patients = [PatientInfo]()
    var filteredPatients = [PatientInfo]()
    let searchController = UISearchController(searchResultsController: nil)
    let patientDAO = PatientDAO.init()
    
    
    deinit{
        print("PatientsViewController Deinitialized.")
    }
    
    //if view is loaded, load and display current patient list from NSMutableArray
    override func viewDidAppear(_ animated: Bool) {
        //add new patient button in navigation tab
        
        
       // self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:
         //   UIColor(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)]
        
        let attrs = [
            NSFontAttributeName: UIFont(name: "Lato-Light", size: 14)!,
            NSForegroundColorAttributeName: UIColor.white
        ]
        
        UINavigationBar.appearance().titleTextAttributes = attrs


        let rightButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(PatientsViewController.addButtonPressed(_:)))
        
        
//        let rightButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(addButtonPressed(sender:)))
        
        self.tabBarController?.navigationItem.rightBarButtonItem = rightButton
        
        //self.navigationController?.navigationBar.tintColor = UIColor.white

        //self.navigationController?.navigationBar.tintColor = UIColor(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        
        reloadPatientList()
    }
    //add new button presses action, pass segue to add patient view controller
    func addButtonPressed(_ sender: AnyObject) {
        //print ("add button pressed")
        performSegue(withIdentifier: "AddPatient", sender: self)
    }
    

    func reloadPatientList(){
        patients.removeAll()
        let loadPatients = patientDAO.getAllPatients()
        //print(loadPatients.count)
        var index = 0
        var tempName : String
        var tempState : Int32
        var tempDob : String
        var tempId : String
        var tempMonitorStartTime : Date
        for object in loadPatients {
            //print("\(object.firstName) \(object.lastName)  \(object.state.rawValue)")
            //tempName = "\(object.initial) \(object.age)"
            tempName = object.initial
            tempState = object.state.rawValue
            tempDob = String(object.age)
            tempId = object.id
            tempMonitorStartTime = object.createDTM as Date
            patients.insert(PatientInfo(name: tempName, status: tempState, dob: tempDob, id: tempId, monitorStartTime: tempMonitorStartTime), at: index)
            index = index+1
        }
        patientList.reloadData()
    }
    
    func clearPatientList() {
        //patients.removeAll()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
       // self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
       // self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Lato-Regular", size: 14)!]
        
        let attrs = [
            NSFontAttributeName: UIFont(name: "Lato-Regular", size: 14)!,
            NSForegroundColorAttributeName: UIColor.white
        ]
 
        UINavigationBar.appearance().titleTextAttributes = attrs
        
        
       // self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:
         //   UIColor(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)]
        

        //add new patient button in navigation tab
        let rightButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(PatientsViewController.addButtonPressed(_:)))
        
        //let rightButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action:#selector(addButtonPressed(sender:)))
        self.tabBarController?.navigationItem.rightBarButtonItem = rightButton
        self.tabBarController?.navigationItem.title = "Patients"
      
        //let navbarFont = UIFont(name: "Lato-Light", size: 10) ?? UIFont.systemFont(ofSize: 10)
        //let barbuttonFont = UIFont(name: "Lato-Light", size: 12) ?? UIFont.systemFont(ofSize: 17)
        
        //UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName:UIColor.white]
        
     //   UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.white], for: UIControlState.normal)

        
        //self.navigationController?.navigationBar.tintColor = UIColor.white
        

        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        
        patientList.tableHeaderView = searchController.searchBar

        searchController.searchBar.scopeButtonTitles = []
        searchController.searchBar.barTintColor = UIColor(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        searchController.searchBar.tintColor = UIColor.gray
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        self.definesPresentationContext = true
        
        reloadPatientList()
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredPatients.count
        }
        return patients.count
        
    }
    func deletePatient(_ sender: UIButton!)
    {
        let indexPathofThisCell = sender.tag
        let patientUid : String
        let patientName : String
        print("this button is at \(indexPathofThisCell) row")
        if searchController.isActive && searchController.searchBar.text != "" {
            patientUid = filteredPatients[indexPathofThisCell].id
            patientName = filteredPatients[indexPathofThisCell].name
        }else{
            patientUid = patients[indexPathofThisCell].id
            patientName = patients[indexPathofThisCell].name
        }
        print("this button is at \(patientUid) row")
        
        
        let alertView = UIAlertController(title: "Delete Patient", message: "Are you sure you want to delete " + patientName+"?", preferredStyle: .alert)
        let confirmed = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
            self.patientDAO.deletePatient(id: patientUid)
            self.reloadPatientList()
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
            print("You've pressed Cancel button");
        }
        alertView.addAction(confirmed)
        alertView.addAction(cancel)
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientListCell") as! PatientListCell
        var patientUid : String
        var patientState : Int32
        var monitorStartTime : Date
        if searchController.isActive && searchController.searchBar.text != "" {
            cell.patientCell = filteredPatients[(indexPath as NSIndexPath).row]
            cell.patientName.text = filteredPatients[(indexPath as NSIndexPath).row].name
            patientUid = filteredPatients[(indexPath as NSIndexPath).row].id
            patientState = filteredPatients[(indexPath as NSIndexPath).row].status
        }else{
            cell.patientCell = patients[(indexPath as NSIndexPath).row]
            cell.patientName.text = patients[(indexPath as NSIndexPath).row].name
            patientUid = patients[(indexPath as NSIndexPath).row].id
            patientState = patients[(indexPath as NSIndexPath).row].status
        }
        if ( (patientState == 1) || (patientState == 2) )
        {
            cell.btnDelete.isHidden = true
        } else {
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = (indexPath as NSIndexPath).row
            cell.btnDelete.addTarget(self, action: #selector(deletePatient), for: .touchUpInside)
        }
        monitorStartTime = patients[(indexPath as NSIndexPath).row].monitorStartTime
        
        _ = Date.timeIntervalSinceReferenceDate
        let timeInterval: Double = Date().timeIntervalSince(monitorStartTime)
        
        //Find the difference between current time and start monitoring time.
        
        //calculate the hours minutes in elapsed time.
        
        let minutes = UInt16(timeInterval / 60.0)
        
        let hours = Int(Double(minutes)/60.0)
        
        let rminutes = Int(Double(minutes).truncatingRemainder(dividingBy: 60.0))
        
        cell.countDown.text = String(hours)+":"+String(rminutes)+" elapsed"
        cell.stylizeView(cell: cell,patientUid: patientUid)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let height: CGFloat = 50
        return height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0 // TODO: see if section headers can be disabled altogether
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredPatients = patients.filter({( pat : PatientInfo) -> Bool in
            return pat.name.lowercased().contains(searchText.lowercased())
        })
        
        patientList.reloadData()
    }
    
    func controllerDidChangeContent(controller: CoreDataStack) {
        patientList.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "TrackingTab"
        {
//            let tabCtrl       = segue.destination as! UITabBarController
//            let tabBarItem = tabCtrl.tabBar.items
//            let tab = (tabBarItem?[1])! as UITabBarItem
//            tab.isEnabled = false
            
            //let detailViewController = tabCtrl.viewControllers!//[0] as! InfectionNavigationController
//            let vc: UITabBarController = segue.destination as! UITabBarController
            let vc = segue.destination as! BaseTabViewVC
            let dvc = vc.viewControllers![0] as! InfectionNavigationController
            //let detailViewController = dvc.topViewController as! InfectionViewController
            
            let detailViewController = dvc.topViewController as! VitalSignsViewController
            
            
            
            if let indexPath = patientList.indexPathForSelectedRow {
                if searchController.isActive && searchController.searchBar.text != ""
                {
                    detailViewController.tabBarController?.navigationItem.title = filteredPatients[indexPath.row].name + ":" + filteredPatients[indexPath.row].dob
                    detailViewController.patientName = filteredPatients[indexPath.row].name
                    detailViewController.patientId = filteredPatients[indexPath.row].id

                }else{
                    //detailViewController.userObject = 100
                    detailViewController.tabBarController?.navigationItem.title = patients[indexPath.row].name + ":" + patients[indexPath.row].dob
                    let patientName = patients[indexPath.row].name
                    detailViewController.patientName = patientName
                    detailViewController.patientId = patients[indexPath.row].id
                    let patientDTO = getPatientObject(patientName)
                    vc.patinentDTO = patientDTO
                    //enableDisableTabIcon(patient: patientDTO!, segue: segue)
              }
            }
            
        }
 
    }
    
    func enableDisableTabIcon(patient:PatientDTO, segue:UIStoryboardSegue)
    {
        let tabCtrl       = segue.destination as! UITabBarController
        let tabBarItem = tabCtrl.tabBar.items
        dump(patient.state)
        
        if patient.state == PatientState.Incomplete
        {
            let threeHourTab = (tabBarItem?[1])! as UITabBarItem
            threeHourTab.isEnabled = false
            
            let sixHourTab = (tabBarItem?[2])! as UITabBarItem
            sixHourTab.isEnabled = false
        }
        else
        {
            //TODO: Get the patient state and see if it is active for the 3 or 6
        }
        
        
    }
    
    func getPatientObject(_ patientName:String) -> PatientDTO?
    {
        let patientDAO = PatientDAO()
        let patientList = patientDAO.getAllPatients()
        
        for patient in patientList
        {
            if (patient.initial == patientName)
            {
                return patient
            }
        }
        return nil
    }

    
}

extension PatientsViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
}

extension PatientsViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
