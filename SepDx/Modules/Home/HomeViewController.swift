//
//  HomeViewController.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 7/5/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit

class HomeViewController: UITabBarController {
    
    
    
    
    deinit{
        print("HomeViewController Deinitialized.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
      
        self.tabBar.barTintColor = UIColor(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        self.tabBar.tintColor = UIColor.black
        
        setUpTabBars()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setUpTabBars(){
        
        let vc1 = UIStoryboard(name: "Patients", bundle: nil).instantiateViewController(withIdentifier: "PatientsViewController")
        //let vc2 = UIStoryboard(name: "Patients", bundle: nil).instantiateViewController(withIdentifier: "StarredPatientsViewController")
        let vc2 = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC")
        //let vc4 = UIStoryboard(name: "Tracking", bundle: nil).instantiateViewControllerWithIdentifier("TrackingViewController")
        
        let firstImage = UIImage(named: "PatientWhite")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let secondImage = UIImage(named: "StarWhite")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        //let thirdImage = UIImage(named: "ClockWhite")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        //let fourthImage = UIImage(named: "TrackingBlue")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        
        
        
        vc1.tabBarItem = UITabBarItem(title: "Patients",image: firstImage,tag: 1)
        vc2.tabBarItem = UITabBarItem(title: "Logout", image: #imageLiteral(resourceName: "logout"),tag: 2)
       // vc3.tabBarItem = UITabBarItem(title: "Notifications",image: thirdImage,tag: 3)
       // vc4.tabBarItem = UITabBarItem(title: "Tracking",image: fourthImage,tag: 3)
        
        //let controllers = [vc1,vc2,vc3,vc4]
        let controllers = [vc1, vc2]
        self.viewControllers = controllers
        
    }
    

    
}
