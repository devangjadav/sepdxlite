//
//  CheckList6Hr.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import CoreData

@objc(CheckList6Hr)
class CheckList6Hr: NSManagedObject {
    
    @NSManaged var cvp: NSNumber?
    @NSManaged var scvO2: NSNumber?
    
    @NSManaged var assessmentOfFluidResponsiveness: String?
    @NSManaged var bedsideCardioVascular: String?
    @NSManaged var repeatFocusedExam: String?
    
    @NSManaged var applyVasoPressors: Bool
    @NSManaged var reassessVolumeStatus: Bool
    @NSManaged var remeasureLactate: Bool
    @NSManaged var conIvFluid: Bool
    @NSManaged var chkPulm: Bool
    @NSManaged var chkOther: Bool
    @NSManaged var patientId: String
    
    @NSManaged var createDTM: NSDate
    //@NSManaged var patientRelation: Patient
    
    convenience init(context: NSManagedObjectContext,cvp: NSNumber?,scvO2: NSNumber?,assessmentOfFluidResponsiveness: String?
        ,bedsideCardioVascular: String?,repeatFocusedExam: String?,applyVasoPressors: Bool,reassessVolumeStatus: Bool,remeasureLactate: Bool, conIvFluid: Bool, chkPulm: Bool, chkOther: Bool, createDTM: NSDate) {
        let entity = NSEntityDescription.entity(forEntityName: "CheckList6Hr",in: context)!
        self.init(entity: entity,insertInto: context)
        self.cvp = cvp
        self.scvO2 = scvO2
        self.assessmentOfFluidResponsiveness = assessmentOfFluidResponsiveness
        self.bedsideCardioVascular = bedsideCardioVascular
        self.repeatFocusedExam = repeatFocusedExam
        self.applyVasoPressors = applyVasoPressors
        self.reassessVolumeStatus = reassessVolumeStatus
        self.remeasureLactate = remeasureLactate
        self.conIvFluid = conIvFluid
        self.chkPulm = chkPulm
        self.chkOther = chkOther
        self.createDTM = createDTM
    }
    
    convenience init(context: NSManagedObjectContext,conIvFluid: Bool, chkPulm: Bool, chkOther: Bool, patientId: String) {
        let entity = NSEntityDescription.entity(forEntityName: "CheckList6Hr",in: context)!
        self.init(entity: entity,insertInto: context)
        
        self.conIvFluid = conIvFluid
        self.chkPulm = chkPulm
        self.chkOther = chkOther
        self.patientId = patientId
        
        self.cvp = 0
        self.scvO2 = 0
        self.assessmentOfFluidResponsiveness = ""
        self.bedsideCardioVascular = ""
        self.repeatFocusedExam = ""
        self.applyVasoPressors = false
        self.reassessVolumeStatus = false
        self.remeasureLactate = false
        self.createDTM = NSDate()
    }
}
