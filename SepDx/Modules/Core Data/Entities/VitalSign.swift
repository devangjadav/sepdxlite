//
//  VitalSign.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import CoreData

@objc(VitalSign)
class VitalSign: NSManagedObject {
    
   
    
    @NSManaged var temperatureSwitch: Bool
    
    @NSManaged var heartRateSwitch: Bool
    @NSManaged var respiratoryRateSwitch: Bool
    @NSManaged var wbcSwitch: Bool
    @NSManaged var patientMeetsCriteria: Bool
    @NSManaged var monitoringStartDTM: Date
    @NSManaged var infectionSwitch: Bool
    
    @NSManaged var createDTM: Date
    @NSManaged var patientId: String
    @NSManaged var sepsisConfirmationDTM: Date
    
    
    convenience init(context: NSManagedObjectContext, infectionSwitch: Bool,temperatureSwitch: Bool, heartRateSwitch: Bool,respiratoryRateSwitch: Bool,wbcSwitch: Bool,patientMeetsCriteria: Bool, monitoringStartDTM: Date, createDTM: Date, patientId: String, sepsisConfirmationDTM: Date) {
        
        let entity = NSEntityDescription.entity(forEntityName: "VitalSign",in: context)!
        self.init(entity: entity,insertInto: context)
        
        self.temperatureSwitch = temperatureSwitch
        self.infectionSwitch = infectionSwitch
        
        self.heartRateSwitch = heartRateSwitch
        self.respiratoryRateSwitch = respiratoryRateSwitch
        self.wbcSwitch = wbcSwitch
        self.patientMeetsCriteria = patientMeetsCriteria
        self.monitoringStartDTM = monitoringStartDTM
        self.createDTM = createDTM
        self.patientId = patientId
        self.sepsisConfirmationDTM = sepsisConfirmationDTM
    }
    

}
