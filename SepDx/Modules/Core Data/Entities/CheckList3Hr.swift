//
//  CheckList3Hr.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import CoreData

@objc(CheckList3Hr)
class CheckList3Hr: NSManagedObject {
    
    @NSManaged var administerAntibiotics: Bool
    @NSManaged var administerCrystalloid: Bool
    @NSManaged var measureLactateLevel: Bool
    @NSManaged var obtainBloodCulture: Bool
    @NSManaged var condLactate: Bool
    @NSManaged var condArterial: Bool
    @NSManaged var condKidney: Bool
    @NSManaged var condPoorPerfusion: Bool
    @NSManaged var condUrineOutput: Bool
    @NSManaged var condMentalStatus: Bool
    @NSManaged var condHyperPerfusion: Bool
    
    @NSManaged var patientId: String
    
    @NSManaged var createDTM: NSDate
    
    
    
    convenience init(context: NSManagedObjectContext,administerAntibiotics: Bool,administerCrystalloid: Bool,measureLactateLevel: Bool,obtainBloodCulture: Bool,createDTM: NSDate, patientId: String) {
        let entity = NSEntityDescription.entity(forEntityName: "CheckList3Hr",in: context)!
        self.init(entity: entity,insertInto: context)
        self.administerAntibiotics = administerAntibiotics
        self.administerCrystalloid = administerCrystalloid
        self.measureLactateLevel = measureLactateLevel
        self.obtainBloodCulture = obtainBloodCulture
        self.createDTM = createDTM
        self.patientId = patientId
    }
}
