//
//  Notification.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import CoreData

@objc(Notification)
class Notification: NSManagedObject {
    
    @NSManaged var message: String
    @NSManaged var createDTM: NSDate

    convenience init(context: NSManagedObjectContext,message: String,createDTM: NSDate) {
        let entity = NSEntityDescription.entity(forEntityName: "Notification",in: context)!
        self.init(entity: entity,insertInto: context)
        self.message = message
        self.createDTM = createDTM
    }
}
