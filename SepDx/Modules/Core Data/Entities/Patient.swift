//
//  Patient.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/2/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import CoreData

@objc(Patient)
class Patient: NSManagedObject {
    
    @NSManaged var initial: String
    @NSManaged var age: Int16
    @NSManaged var weight: String
    @NSManaged var gender: String
    @NSManaged var id: String
    @NSManaged var state: PatientState
    @NSManaged var favorite: Bool
    @NSManaged var createDTM: NSDate
    @NSManaged var startMonitoring: NSDate
    @NSManaged var checklist3HourComplete: Bool
    @NSManaged var checklist6HourComplete: Bool
    
    @NSManaged var vitalSignRelation: VitalSign?
    @NSManaged var checkList3HrRelation: CheckList3Hr?
    @NSManaged var checkList6HrRelation: CheckList6Hr?
    
    
    convenience init(context: NSManagedObjectContext,initial: String, age: Int16, weight: String, gender: String, state: PatientState,favorite: Bool) {
        let entity = NSEntityDescription.entity(forEntityName: "Patient",in: context)!
        self.init(entity: entity,insertInto: context)
        self.initial = initial
        self.age = age
        self.weight = weight
        self.gender = gender
        self.id = UUID().uuidString
        self.state = state
        self.favorite = favorite
        self.createDTM = Date() as NSDate
    }
    
    convenience init(context: NSManagedObjectContext,id: String, startMonitoring: NSDate) {
        let entity = NSEntityDescription.entity(forEntityName: "Patient",in: context)!
        self.init(entity: entity,insertInto: context)
        self.id = id
        self.startMonitoring = startMonitoring
    }

}
