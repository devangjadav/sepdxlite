//
//  CheckList3HrDTO.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation

class CheckList3HrDTO: NSObject{
    
    var administerAntibiotics: Bool
    var administerCrystalloid: Bool
    var measureLactateLevel: Bool
    var obtainBloodCulture: Bool
    var condLactate: Bool
    var condArterial: Bool
    var condKidney: Bool
    var condPoorPerfusion: Bool
    var condUrineOutput: Bool
    var condMentalStatus: Bool
    var condHyperPerfusion: Bool

    var patientId: String
    
    var createDTM: NSDate
    
    init(administerAntibiotics: Bool,administerCrystalloid: Bool,measureLactateLevel: Bool,obtainBloodCulture: Bool,createDTM: NSDate, patientId: String){
        
        self.administerAntibiotics = administerAntibiotics
        self.administerCrystalloid = administerCrystalloid
        self.measureLactateLevel = measureLactateLevel
        self.obtainBloodCulture = obtainBloodCulture
        self.condKidney = false
        self.condLactate = false
        self.condArterial = false
        self.condPoorPerfusion = false
        self.condUrineOutput = false
        self.condMentalStatus = false
        self.condHyperPerfusion = false
        self.createDTM = createDTM
        self.patientId = patientId
    }
    
    init(administerAntibiotics: Bool,administerCrystalloid: Bool,measureLactateLevel: Bool,obtainBloodCulture: Bool, condLactate: Bool, condArterial: Bool, condKidney: Bool, condPoorPerfusion: Bool, condUrineOutput: Bool, condMentalStatus: Bool, condHyperPerfusion: Bool, createDTM: NSDate, patientId: String){
        
        self.administerAntibiotics = administerAntibiotics
        self.administerCrystalloid = administerCrystalloid
        self.measureLactateLevel = measureLactateLevel
        self.obtainBloodCulture = obtainBloodCulture
        self.condKidney = condKidney
        self.condLactate = condLactate
        self.condArterial = condArterial
        self.condPoorPerfusion = condPoorPerfusion
        self.condUrineOutput = false
        self.condMentalStatus = false
        self.condHyperPerfusion = false
        self.createDTM = createDTM
        self.patientId = patientId
    }

    
    convenience init(cList:CheckList3Hr){
        self.init(administerAntibiotics: cList.administerAntibiotics,administerCrystalloid: cList.administerCrystalloid,measureLactateLevel: cList.measureLactateLevel,obtainBloodCulture: cList.obtainBloodCulture, condLactate: cList.condLactate, condArterial: cList.condArterial, condKidney: cList.condKidney, condPoorPerfusion: cList.condPoorPerfusion, condUrineOutput: cList.condUrineOutput, condMentalStatus: cList.condMentalStatus, condHyperPerfusion: cList.condHyperPerfusion, createDTM: cList.createDTM,
                  patientId: cList.patientId)
    }
    
    
}

