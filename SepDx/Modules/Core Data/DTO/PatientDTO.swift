//
//  PatientDTO.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/2/16.
//  Copyright © 2016 MAN. All rights reserved.
//


import Foundation


class PatientDTO: NSObject {
    
    var initial: String
    var weight: String
    var age: Int16
    var gender: String
    //var firstName: String
    //var lastName: String
    //var dateOfBirth: String
    var id:String
    var state:PatientState
    var favorite: Bool
    var createDTM: Date
    var startMonitoring: Date
    var checklist3HourComplete: Bool
    var checklist6HourComplete: Bool
    
    /*
     init(firstName: String,lastName: String,dateOfBirth: String,id: String, state: PatientState,favorite: Bool, createDTM: NSDate, startMonitoring: NSDate){
     
     self.firstName = firstName
     self.lastName = lastName
     self.dateOfBirth = dateOfBirth
     self.id = id
     self.state = state
     self.favorite = favorite
     self.createDTM = createDTM
     self.startMonitoring = startMonitoring
     }
     */
    init(initial: String, age: Int16, weight: String, gender: String, id: String, state: PatientState,favorite: Bool, createDTM: Date, startMonitoring: Date, checklist3HourComplete: Bool, checklist6HourComplete: Bool){
        
        self.initial = initial
        self.age = age
        self.weight = weight
        self.gender = gender
        self.id = id
        self.state = state
        self.favorite = favorite
        self.createDTM = createDTM
        self.startMonitoring = startMonitoring
        self.checklist3HourComplete = checklist3HourComplete
        self.checklist6HourComplete = checklist6HourComplete
    }
    
    convenience init(patient:Patient){
        //        self.init(firstName:patient.firstName,lastName:patient.lastName,dateOfBirth: patient.dateOfBirth,id: patient.id, state: patient.state,favorite: patient.favorite, createDTM: patient.createDTM, startMonitoring: patient.startMonitoring)
        self.init(initial:patient.initial,age:patient.age,weight: patient.weight, gender: patient.gender, id: patient.id, state: patient.state,favorite: patient.favorite, createDTM: patient.createDTM as Date, startMonitoring: patient.startMonitoring as Date, checklist3HourComplete: patient.checklist3HourComplete as Bool, checklist6HourComplete: patient.checklist6HourComplete as Bool)
        
    }
    
    
}
