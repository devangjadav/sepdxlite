//
//  CheckList6HrDTO.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation

class CheckList6HrDTO: NSObject{
    
    var cvp: NSNumber?
    var scvO2: NSNumber?
    
    var assessmentOfFluidResponsiveness: String?
    var bedsideCardioVascular: String?
    var repeatFocusedExam: String?
    
    var applyVasoPressors: Bool
    var reassessVolumeStatus: Bool
    var remeasureLactate: Bool
    var conIvFluid: Bool
    var chkPulm: Bool
    var chkOther: Bool
    var patientId: String
    
    var createDTM: NSDate
    
    init(cvp: NSNumber?,scvO2: NSNumber?,assessmentOfFluidResponsiveness: String?
        ,bedsideCardioVascular: String?,repeatFocusedExam: String?,applyVasoPressors: Bool,reassessVolumeStatus: Bool,remeasureLactate: Bool, conIvFluid: Bool, chkPulm: Bool, chkOther: Bool, createDTM: NSDate, patientId: String){
    
        self.cvp = cvp
        self.scvO2 = scvO2
        self.assessmentOfFluidResponsiveness = assessmentOfFluidResponsiveness
        self.bedsideCardioVascular = bedsideCardioVascular
        self.repeatFocusedExam = repeatFocusedExam
        self.applyVasoPressors = applyVasoPressors
        self.reassessVolumeStatus = reassessVolumeStatus
        self.remeasureLactate = remeasureLactate
        self.createDTM = createDTM
        self.patientId = patientId
        
        self.conIvFluid = conIvFluid
        self.chkPulm = chkPulm
        self.chkOther = chkOther
        

    }
    
    init(conIvFluid: Bool,chkPulm: Bool,chkOther: Bool, patientId: String){
        
        //self.cvp = cvp
        //self.scvO2 = scvO2
        //self.assessmentOfFluidResponsiveness = assessmentOfFluidResponsiveness
        //self.bedsideCardioVascular = bedsideCardioVascular
        //self.repeatFocusedExam = repeatFocusedExam
        self.applyVasoPressors = false
        self.reassessVolumeStatus = false
        self.remeasureLactate = false
        self.createDTM = NSDate()
        
       
        self.conIvFluid = conIvFluid
        self.chkPulm = chkPulm
        self.chkOther = chkOther
        self.patientId = patientId
        
        
    }
    
    convenience init(cList:CheckList6Hr){
        self.init(cvp: cList.cvp,scvO2: cList.scvO2,assessmentOfFluidResponsiveness: cList.assessmentOfFluidResponsiveness
            ,bedsideCardioVascular: cList.bedsideCardioVascular,repeatFocusedExam: cList.repeatFocusedExam,applyVasoPressors: cList.applyVasoPressors,reassessVolumeStatus: cList.reassessVolumeStatus,remeasureLactate: cList.remeasureLactate,conIvFluid: cList.conIvFluid, chkPulm: cList.chkPulm, chkOther: cList.chkOther, createDTM: cList.createDTM, patientId: cList.patientId)
    }
    
    
}
