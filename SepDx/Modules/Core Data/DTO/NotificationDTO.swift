//
//  NotificationDTO.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation

class NotificationDTO: NSObject {
    
    var message: String
    var createDTM: NSDate
    
    init(message: String,createDTM: NSDate){
        
        self.message = message
        self.createDTM = createDTM
    }
    
    convenience init(not:Notification){
        self.init(message: not.message,createDTM: not.createDTM)
    }
    
    
}