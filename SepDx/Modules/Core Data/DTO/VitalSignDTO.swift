//
//  VitalSignDTO.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation

class VitalSignDTO: NSObject{
    
    var temperatureSwitch: Bool
    
    var heartRateSwitch: Bool
    var respiratoryRateSwitch: Bool
    var wbcSwitch: Bool
    var infectionSwitch: Bool
    
    
    var patientMeetsCriteria: Bool
    var monitoringStartDTM: Date
    var createDTM: Date
    var patientId: String
    var sepsisConfirmationDTM: Date
    
    init(temperatureSwitch: Bool, heartRateSwitch: Bool,infectionSwitch: Bool,respiratoryRateSwitch: Bool,wbcSwitch: Bool, patientMeetsCriteria: Bool, monitoringStartDTM: Date, createDTM: Date, patientId: String, sepsisConfirmationDTM: Date){
        
        self.temperatureSwitch = temperatureSwitch
        self.infectionSwitch = infectionSwitch
        self.heartRateSwitch = heartRateSwitch
        self.respiratoryRateSwitch = respiratoryRateSwitch
        self.wbcSwitch = wbcSwitch
        
        
        self.patientMeetsCriteria = patientMeetsCriteria
        self.monitoringStartDTM = monitoringStartDTM
        self.createDTM = createDTM
        self.patientId = patientId
        self.sepsisConfirmationDTM = sepsisConfirmationDTM
        
    }
    
    init(temperatureSwitch: Bool, heartRateSwitch: Bool,infectionSwitch: Bool,respiratoryRateSwitch: Bool,wbcSwitch: Bool, patientMeetsCriteria: Bool, monitoringStartDTM: Date, createDTM: Date, patientId: String){
        
        self.temperatureSwitch = temperatureSwitch
        self.infectionSwitch = infectionSwitch
        self.heartRateSwitch = heartRateSwitch
        self.respiratoryRateSwitch = respiratoryRateSwitch
        self.wbcSwitch = wbcSwitch
        
        
        self.patientMeetsCriteria = patientMeetsCriteria
        self.monitoringStartDTM = monitoringStartDTM
        self.createDTM = createDTM
        self.patientId = patientId
        self.sepsisConfirmationDTM = NSDate.distantPast
        
    }

    
    convenience init(vSign:VitalSign){
        self.init(temperatureSwitch: vSign.temperatureSwitch, heartRateSwitch: vSign.heartRateSwitch, infectionSwitch: vSign.infectionSwitch,respiratoryRateSwitch: vSign.respiratoryRateSwitch,wbcSwitch: vSign.wbcSwitch,patientMeetsCriteria: vSign.patientMeetsCriteria,monitoringStartDTM: vSign.monitoringStartDTM as Date, createDTM: vSign.createDTM as Date, patientId: vSign.patientId, sepsisConfirmationDTM: vSign.sepsisConfirmationDTM as Date)
    }
    
    
}
