//
//  PatientDAO.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/2/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import CoreData


@objc enum PatientState: Int32
{
    case Incomplete = 0 //yellow, patient created, details entered, clinician not agreed with system assessment, not started monitoring
    case Active     = 1 //green - activerly monitored, ongoing 3 hour or 6 hour timer
    case Overdue    = 2 //red - 3 hour timer elapsed, checklist not completed
    case Moncomplete    = 3 //green with check, checklist completed for both 3 hour and 6 hour timer
    case postOverdue    = 4  // red with check, 6 hour timer elapsed, checklist not completed
}


protocol PatientDAOContract{
    
    func addPatient(initial:String, age: Int16, weight: String, gender: String)
    func deletePatient(id:String)
    func getAllPatients()->[PatientDTO]
    //func validatePatientExists(patientFirstName: String, patientLastName: String, patientDob: String)->Bool
    func validatePatientExists(_ patientInitial: String, patientAge: Int16)->Bool
    func addVitalSignsForPatient(_ vitalSigns: VitalSignDTO)
    func loadVitals(_ patientId: String)->VitalSignDTO
    func saveInfection(_ infectionSwitch: Bool, patientId: String)
    func convBooleanToInt(passBool: Bool) -> Int
    func convIntToBoolean(passInt: Int) -> Bool
    func startMonitoring(_ vitalSigns: VitalSignDTO)
    func saveHypoPerfusion(_ condLactate: Bool, condKidney: Bool, condArterial: Bool, condPoorPerfusion: Bool, patientId: String)
    func saveHyperPerfusion2(_ condUrineOutput: Bool, condMentalStatus: Bool, condHyperPerfusion: Bool, patientId: String)
    func saveShockTimer(conIvFluid: Bool, chkPulm: Bool, chkOther: Bool, patientId: String)
    func loadChecklist6Hour(patientId: String) ->CheckList6HrDTO
    /*
    func setFavoriteStatus()
    func getAllFavoritePatients()
    
    func addVitalSignForPatient()
    func add3HrCheckListForPatient()
    func add6HrCheckListForPatient()
    */
}

class PatientDAO:PatientDAOContract{
    
    var sharedContext: NSManagedObjectContext {
        return CoreDataStack.sharedInstance().managedObjectContext!
    }
    
    init(){
        
    }

    func addPatient(initial: String, age: Int16, weight: String, gender: String){
        
        
        let _ = Patient(context: sharedContext, initial:initial, age: age, weight: weight, gender: gender, state: PatientState.Incomplete,favorite: true)
        
        CoreDataStack.sharedInstance().saveContext()
        
    }

    func deletePatient(id: String)
    {
        let fetchRequest = NSFetchRequest<Patient>(entityName: "Patient")

        let firstPredicate = NSPredicate(format: "id == %@", id)
        fetchRequest.predicate = firstPredicate
        do {
            let fetchedEntities = try self.sharedContext.fetch(fetchRequest)
            if let entityToDelete = fetchedEntities.first {
                self.sharedContext.delete(entityToDelete as NSManagedObject)
            }
        } catch {
            // Do something in response to error condition
        }
        CoreDataStack.sharedInstance().saveContext()
    }
    
    //Validates combination of initial, age. If all three attributes matches then
    //returns "false" for alerting the user otherwise returns "true" for adding a new patient
    func validatePatientExists(_ patientInitial: String, patientAge: Int16)-> Bool{
        
       // let fetchRequest = NSFetchRequest(entityName: "Patient")
        let fetchRequest = NSFetchRequest<Patient>(entityName: "Patient")
        
        fetchRequest.returnsObjectsAsFaults = false
        // Create a new predicates that build "And" condition required for search the fname,lname and dob
        let firstPredicate = NSPredicate(format: "initial == %@", patientInitial)
        let secondPredicate = NSPredicate(format: "age == %@", String(patientAge))
        
        // Combine the three predicates above in to one compound predicate with "And" condition
        let predicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [firstPredicate, secondPredicate])
        
        // Set the predicate on the fetch request
        fetchRequest.predicate = predicate
        var results:NSArray
        results = try! sharedContext.fetch(fetchRequest) as NSArray
        //if patient is found then return false to show alert, else add as a new patient
        if (results.count > 0){
            return false
        } else {
            return true
        }
    }
    
    func getAllPatients()->[PatientDTO]{
    
//       let fetchRequest = NSFetchRequest(entityName: "Patient")
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Patient")
        
        
//        if #available(iOS 10.0, *) {
//            let request: NSFetchRequest<Patient> = Patient.fetchRequest() as! NSFetchRequest<Patient>
//        } else {
//           return
//        }
        var results:[Patient]?
        
        //by default order patientlist by patient create date, LIFO
        let sortDescriptor = NSSortDescriptor(key: "createDTM", ascending: false)
        // Set the list of sort descriptors in the fetch request,
        // so it includes the sort descriptor
        fetchRequest.sortDescriptors = [sortDescriptor]
        results = try! sharedContext.fetch(fetchRequest) as! [Patient]
        let patientList = results?.map({patient in
            PatientDTO(
                patient: patient
               )
        })
        return patientList ?? []

    }
    
    func loadVitals(_ patientId: String)->VitalSignDTO {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "VitalSign")
        fetchRequest.predicate = NSPredicate(format: "patientId ==%@", patientId)
        var result:[VitalSign]
        result = try! sharedContext.fetch(fetchRequest) as! [VitalSign]
        var vitalSigns: VitalSignDTO
        if (result.count != 0) {
            vitalSigns =  VitalSignDTO(vSign:result[result.count-1]) //fix this later
            
        }
        else {
            vitalSigns = VitalSignDTO(temperatureSwitch: false, heartRateSwitch: false, infectionSwitch: false, respiratoryRateSwitch: false, wbcSwitch: false, patientMeetsCriteria: false, monitoringStartDTM: Date(), createDTM: Date(), patientId: "", sepsisConfirmationDTM: Date())
            
        }
        return vitalSigns
        
    }
    
    func addVitalSignsForPatient(_ vitalSigns: VitalSignDTO) {
        //Save the Vital signs
        let _ = VitalSign(context: sharedContext, infectionSwitch: vitalSigns.infectionSwitch, temperatureSwitch: vitalSigns.temperatureSwitch,  heartRateSwitch: vitalSigns.heartRateSwitch, respiratoryRateSwitch: vitalSigns.respiratoryRateSwitch, wbcSwitch: vitalSigns.wbcSwitch, patientMeetsCriteria: vitalSigns.patientMeetsCriteria, monitoringStartDTM: vitalSigns.monitoringStartDTM, createDTM: vitalSigns.createDTM, patientId: vitalSigns.patientId, sepsisConfirmationDTM: vitalSigns.sepsisConfirmationDTM)
        
        CoreDataStack.sharedInstance().saveContext()
        
    }
    
    func startMonitoring(_ vitalSigns: VitalSignDTO) {
        let fetchRequest = NSFetchRequest<Patient>(entityName: "Patient")
        
        let firstPredicate = NSPredicate(format: "id == %@", vitalSigns.patientId)
        fetchRequest.predicate = firstPredicate
        do {
            let fetchedEntities = try self.sharedContext.fetch(fetchRequest)
            if let entityToUpdate = fetchedEntities.first {
                entityToUpdate.startMonitoring = vitalSigns.monitoringStartDTM as NSDate
            }
        }
        catch {
            //catch the error
        }
        
        CoreDataStack.sharedInstance().saveContext()
        

    }
    
    func saveInfection(_ infectionSwitch: Bool, patientId: String){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "VitalSign")
        fetchRequest.predicate = NSPredicate(format: "patientId ==%@", patientId)
        var vitalSigns:[VitalSign]
        vitalSigns = try! sharedContext.fetch(fetchRequest) as! [VitalSign]
        if(vitalSigns.count != 0) {
            vitalSigns[0].infectionSwitch = infectionSwitch
           
            let _ = VitalSign(context: sharedContext, infectionSwitch: vitalSigns[0].infectionSwitch, temperatureSwitch: vitalSigns[0].temperatureSwitch,  heartRateSwitch: vitalSigns[0].heartRateSwitch, respiratoryRateSwitch: vitalSigns[0].respiratoryRateSwitch, wbcSwitch: vitalSigns[0].wbcSwitch, patientMeetsCriteria: vitalSigns[0].patientMeetsCriteria, monitoringStartDTM: vitalSigns[0].monitoringStartDTM, createDTM: vitalSigns[0].createDTM, patientId: vitalSigns[0].patientId, sepsisConfirmationDTM: vitalSigns[0].sepsisConfirmationDTM)
        }
        else {
            let _ = VitalSign(context: sharedContext, infectionSwitch: infectionSwitch, temperatureSwitch: false,  heartRateSwitch: false, respiratoryRateSwitch: false, wbcSwitch: false, patientMeetsCriteria: false, monitoringStartDTM: Date(), createDTM: Date(), patientId: patientId, sepsisConfirmationDTM: Date())
            
        }
        CoreDataStack.sharedInstance().saveContext()
        
    }

    func convBooleanToInt(passBool: Bool) -> Int
    {
        if (passBool)
        {
            return 1
        } else {
            return 0
        }
    }
    
    func convIntToBoolean(passInt: Int) -> Bool
    {
        if (passInt == 1)
        {
            return true
        } else {
            return false
        }
    }
    
    func savePatientDetails(patientDTO: PatientDTO)
    {
        
    }
    func loadChecklist3Hour(_ patientId: String)->CheckList3HrDTO {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CheckList3Hr")
        fetchRequest.predicate = NSPredicate(format: "patientId ==%@", patientId)
        var result:[CheckList3Hr]
        result = try! sharedContext.fetch(fetchRequest) as! [CheckList3Hr]
        var checkList: CheckList3HrDTO
        if (result.count != 0) {
            checkList =  CheckList3HrDTO(cList:result[0]) //fix this later
                
        }
        else {
            checkList = CheckList3HrDTO(administerAntibiotics: false, administerCrystalloid: false, measureLactateLevel: false, obtainBloodCulture: false, createDTM: NSDate(), patientId: patientId)
                
            }
            return checkList
            
        }
    func saveChecklist3Hour(_ administerAntibiotics: Bool, measureLactateLevel: Bool, obtainBloodCulture: Bool, patientId: String){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CheckList3Hr")
        fetchRequest.predicate = NSPredicate(format: "patientId ==%@", patientId)
        var checkLists:[CheckList3Hr]
        checkLists = try! sharedContext.fetch(fetchRequest) as! [CheckList3Hr]
        if(checkLists.count != 0) {
            checkLists[0].administerAntibiotics = administerAntibiotics
            checkLists[0].measureLactateLevel = measureLactateLevel
            checkLists[0].obtainBloodCulture = obtainBloodCulture
            
            CoreDataStack.sharedInstance().saveContext()
        }
        else {
            let _ = CheckList3Hr(context: sharedContext, administerAntibiotics: administerAntibiotics, administerCrystalloid: false, measureLactateLevel: measureLactateLevel, obtainBloodCulture: obtainBloodCulture, createDTM: NSDate(), patientId: patientId)
            CoreDataStack.sharedInstance().saveContext()
            
        }
    }
    
        func saveHypoPerfusion(_ condLactate: Bool, condKidney: Bool, condArterial: Bool, condPoorPerfusion: Bool, patientId: String){
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CheckList3Hr")
            fetchRequest.predicate = NSPredicate(format: "patientId ==%@", patientId)
            var checkLists:[CheckList3Hr]
            checkLists = try! sharedContext.fetch(fetchRequest) as! [CheckList3Hr]
            if(checkLists.count != 0) {
                checkLists[0].condKidney = condKidney
                checkLists[0].condLactate = condLactate
                checkLists[0].condArterial = condArterial
                checkLists[0].condPoorPerfusion = condPoorPerfusion
                
                CoreDataStack.sharedInstance().saveContext()
            }
            else {
                // do nothing - this condition should not arise.
                
            }
    }
    
    func saveHyperPerfusion2(_ condUrineOutput: Bool, condMentalStatus: Bool, condHyperPerfusion: Bool, patientId: String){
                
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CheckList3Hr")
            fetchRequest.predicate = NSPredicate(format: "patientId ==%@", patientId)
            var checkLists:[CheckList3Hr]
            checkLists = try! sharedContext.fetch(fetchRequest) as! [CheckList3Hr]
            if(checkLists.count != 0) {
                checkLists[0].condUrineOutput = condUrineOutput
                checkLists[0].condMentalStatus = condMentalStatus
                checkLists[0].condHyperPerfusion = condHyperPerfusion
                    
                CoreDataStack.sharedInstance().saveContext()
            }
            else {
                    // do nothing - this condition should not arise.
                    
            }
    
        
    }
    
    func saveShockTimer(conIvFluid: Bool, chkPulm: Bool, chkOther: Bool, patientId: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CheckList6Hr")
        fetchRequest.predicate = NSPredicate(format: "patientId ==%@", patientId)
        var checkLists:[CheckList6Hr]
        checkLists = try! sharedContext.fetch(fetchRequest) as! [CheckList6Hr]
        if(checkLists.count != 0) {
            checkLists[0].conIvFluid = conIvFluid
            checkLists[0].chkOther = chkOther
            checkLists[0].chkPulm = chkPulm
            
            CoreDataStack.sharedInstance().saveContext()
        }
        else {
            let _ = CheckList6Hr(context: sharedContext, conIvFluid: conIvFluid, chkPulm: chkPulm, chkOther: chkOther, patientId: patientId)
            CoreDataStack.sharedInstance().saveContext()
            
        }

        
        
    }
    
    func loadChecklist6Hour(patientId: String) ->CheckList6HrDTO {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CheckList6Hr")
        fetchRequest.predicate = NSPredicate(format: "patientId ==%@", patientId)
        var result:[CheckList6Hr]
        result = try! sharedContext.fetch(fetchRequest) as! [CheckList6Hr]
        var checkList: CheckList6HrDTO
        if (result.count != 0) {
            checkList =  CheckList6HrDTO(cList:result[result.count-1]) //fix this later
            
        }
        else {
            checkList = CheckList6HrDTO(conIvFluid: false, chkPulm: false, chkOther: false, patientId: patientId)
            
        }
        return checkList
        
    }

    func secondsToHoursMinutesSeconds (s : Int) -> (Int, Int, Int) {
        return (s / 3600, (s % 3600) / 60, (s % 3600) % 60)
    }


  
}
