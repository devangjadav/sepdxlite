//
//  NotificationDAO.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 8/7/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import CoreData


protocol NotificationDAOContract{
    
     func getAllNotifications()->[NotificationDTO]
     func addNotification()
    
}

class NotificationDAO:NotificationDAOContract{

    init(){
        
    }
    
    func getAllNotifications()->[NotificationDTO]{
        
        return [NotificationDTO]()
    }
    
    func addNotification(){
        
        
    }
    
}