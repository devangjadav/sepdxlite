//
//  VitalSignsViewController.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 7/18/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit

class VitalSignsViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var saveButton: UIButton!
    var userObject: Int = 0
    var patientName:String = ""
    var patientId:String = ""

    
    
    @IBOutlet weak var heartRateSwitch: UISegmentedControl!
    @IBOutlet weak var temperatureSwitch: UISegmentedControl!
    @IBOutlet weak var wbcSwitch: UISegmentedControl!
    @IBOutlet weak var respiratoryRateSwitch: UISegmentedControl!
    
      @IBOutlet weak var sepsisCriteria: UILabel!
    
    var infectionSwitch: Bool = true
    
    var temp: Bool = false
    var wbc: Bool = false
    var rr: Bool = false
    var hr: Bool = false
    var isSpesisCriteriaMet = false
    
    let patientDAO = PatientDAO.init()
    
    
    deinit{
        print("VitalSignsViewController Deinitialized.")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        

        super.viewWillAppear(animated)
        //self.tabBarController?.navigationItem.title = "My Title"
       // self.tabBarController?.navigationItem.leftBarButtonItem = settingsButton //This is the IBOutlet variable that you previously added
        loadVitals()

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.title = "Tracking Patients"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userObject)
        
        
        //update segment font color
        //UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.red], for: UIControlState.selected)

        
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)], for: UIControlState.normal)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        (heartRateSwitch.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (heartRateSwitch.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        
        (temperatureSwitch.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (temperatureSwitch.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)

        
        (respiratoryRateSwitch.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (respiratoryRateSwitch.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        
        (wbcSwitch.subviews[0] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        (wbcSwitch.subviews[1] as UIView).tintColor = UIColor.init(red: 28/255.0, green: 56/255.0, blue: 105/255.0, alpha: 1)
        
        // Do any additional setup after loading the view, typically from a nib.
        //nextButton.isHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VitalSignsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        print(patientName)
        loadVitals()
        
        
        
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func saveBtnClicked(_ sender: AnyObject) {
        
        
    
           var vitalCount: Int = 0
        
        if (self.heartRateSwitch.selectedSegmentIndex == 1) {
            vitalCount = vitalCount + 1
            hr = true
        }
        if (self.respiratoryRateSwitch.selectedSegmentIndex == 1) {
            vitalCount = vitalCount + 1
            rr = true
        }
        if (self.temperatureSwitch.selectedSegmentIndex == 1) {
            vitalCount = vitalCount + 1
            temp = true
        }
        if (self.wbcSwitch.selectedSegmentIndex == 1) {
            vitalCount = vitalCount + 1
            wbc = true
        }
        
        if vitalCount >= 2 {
            
            
            
            
            let currentDate: Date = Date()
            
            let vitalSignDTO = VitalSignDTO( temperatureSwitch: temp, heartRateSwitch: hr, infectionSwitch: infectionSwitch,respiratoryRateSwitch: rr, wbcSwitch: wbc, patientMeetsCriteria: true, monitoringStartDTM: currentDate, createDTM: currentDate, patientId: patientId)
            let patientDao = PatientDAO()
            patientDao.addVitalSignsForPatient(vitalSignDTO)
            if (infectionSwitch) {
                //take the user to screen 7
                isSpesisCriteriaMet = true
                //performSegue(withIdentifier: "VitalToConfirmationTab", sender: self)
                patientDao.startMonitoring(vitalSignDTO)
                let notificationFacade = NotificationFacade()
                notificationFacade.publishNotification(patientName)
                

            }
            else {//vitalCount >= 2 but no infection
                
                //take the user to screen 25
                 isSpesisCriteriaMet = false
                //performSegue(withIdentifier: "VitalToScreen25Tab", sender: self)
                

            }
                    }
        else {
            if (infectionSwitch) { //vital count < 2 but infection suspected
                //take the user to screen 24
                //performSegue(withIdentifier: "VitalToScreen24Tab", sender: self)

            }
            else { //vital count < 2 and no infection suspected
                //take the user to home page?
            }
            
        }
        //performSegue(withIdentifier: "Timers", sender: self)
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "VitalToScreen25Tab" {
            if (isSpesisCriteriaMet) {
                return false
            }
            else {
                return true
            }
        } else if identifier == "VitalToConfirmationTab" {
            if (isSpesisCriteriaMet) {
                return true
            }
            else {
                return false
            }
            
        }
        return true
    }
    func loadVitals () {
        let vitals = patientDAO.loadVitals(self.patientId)
        self.temperatureSwitch.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: vitals.temperatureSwitch)
        self.respiratoryRateSwitch.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: vitals.respiratoryRateSwitch)
        self.wbcSwitch.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: vitals.wbcSwitch)
        self.heartRateSwitch.selectedSegmentIndex = patientDAO.convBooleanToInt(passBool: vitals.heartRateSwitch)
    }

    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "VitalToConfirmationTab"
        {
            
            //let detailViewController = tabCtrl.viewControllers!//[0] as! InfectionNavigationController
            //let vc: UITabBarController = segue.destination as! UITabBarController
            // let dvc = vc.viewControllers![0] as! InfectionNavigationController
            let detailViewController = segue.destination as! ConfirmationViewController
            detailViewController.tabBarController?.navigationItem.title = self.patientName
            detailViewController.patientName = self.patientName
            detailViewController.patientId = self.patientId
            
            
        }
        else if segue.identifier == "VitalToScreen25Tab"
        {
            
            //let detailViewController = tabCtrl.viewControllers!//[0] as! InfectionNavigationController
            //let vc: UITabBarController = segue.destination as! UITabBarController
            // let dvc = vc.viewControllers![0] as! InfectionNavigationController
            let detailViewController = segue.destination as! Screen25ViewController
            detailViewController.tabBarController?.navigationItem.title = self.patientName
            detailViewController.patientName = self.patientName
            detailViewController.patientId = self.patientId
            
            
        }
 
    }*/

    
}
