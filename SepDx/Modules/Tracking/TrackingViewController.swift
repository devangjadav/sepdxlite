//
//  TrackingViewController.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 7/5/16.
//  Copyright © 2016 MAN. All rights reserved.
//
import UIKit

enum TrackingContentType: Int {
    case VitalSigns
    case CheckList3Hr
    case CheckList6Hr
}

class TrackingViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var contentContainer: UIView!
    
    deinit{
        print("PatientsViewController Deinitialized.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        
        // Set navigation bar background colour
        //self.navigationController!.navigationBar.barTintColor = UIColor.yellowColor()
        
        //let attributes = [NSFontAttributeName : UIFont(name: "System", size: 16)!, NSForegroundColorAttributeName : UIColor.whiteColor()]
       // self.navigationController!.navigationBar.titleTextAttributes = attributes
        // Do any additional setup after loading the view, typically from a nib.
         showContentController(type: .VitalSigns)
    }
    
    override func viewDidAppear(_ animated: Bool) {
         segmentedControl.removeBorders()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func segmentChanged(sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            showContentController(type: .VitalSigns)
        }else if sender.selectedSegmentIndex == 1 {
            showContentController(type: .CheckList3Hr)
        }else if sender.selectedSegmentIndex == 2 {
            showContentController(type: .CheckList6Hr)
        }
    }
    
    private func showContentController(type: TrackingContentType?) {
        if let type = type {
            switch (type) {
            case .VitalSigns:
                let vc = UIStoryboard(name: "Tracking", bundle: nil).instantiateViewController(withIdentifier: "InfectionViewController")
                loadContentController(content: vc)
                
            case .CheckList3Hr:
                let vc = UIStoryboard(name: "Tracking", bundle: nil).instantiateViewController(withIdentifier: "CheckList3HrViewController")
                loadContentController(content: vc)
                
            case .CheckList6Hr:
                let vc = UIStoryboard(name: "Tracking", bundle: nil).instantiateViewController(withIdentifier: "CheckList6HrViewController")
                loadContentController(content: vc)
            }
        }
    }
    
    private func loadContentController(content: UIViewController) {
        self.addChildViewController(content)
        content.view.frame = contentContainer.bounds
        contentContainer.addSubview(content.view)
        content.didMove(toParentViewController: self)
    }
    
    
}
