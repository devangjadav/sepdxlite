//
//  CheckList3HrViewController.swift
//  SepDx
//
//  Created by Vignarajan Ravindran on 7/18/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import UIKit

class CheckList3HrViewController: UIViewController {
    
    
    
    
    deinit{
        print("CheckList3HrViewController Deinitialized.")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "3 Hour CheckList"
        // self.tabBarController?.navigationItem.leftBarButtonItem = settingsButton //This is the IBOutlet variable that you previously added
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
