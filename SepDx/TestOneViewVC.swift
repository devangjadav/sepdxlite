//
//  TestOneViewVC.swift
//  SepDx
//
//  Created by Nguyen Le on 9/20/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import UIKit

class TestOneViewVC: UIViewController
{
    override func viewDidLoad()
    {
//        print(self.parent?.parent.debugDescription)
        let tabCtrl       = self.parent?.parent as! UITabBarController
        let tabBarItem = tabCtrl.tabBar.items
        let tab = (tabBarItem?[1])! as UITabBarItem
        tab.isEnabled = true
    }
    
    @IBAction func homeBtnPressed(_ sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeNav")
        self.present(vc, animated: true, completion: nil)
    }
}
