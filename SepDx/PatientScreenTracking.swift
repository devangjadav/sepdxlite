//
//  PatientScreenTracking.swift
//  SepDx
//
//  Created by Nidhi Narang on 9/23/16.
//  Copyright © 2016 MAN. All rights reserved.
//

import Foundation
import CoreData

@objc(PatientScreenTracking)
class PatientScreenTracking: NSManagedObject {
    
    @NSManaged var patientId: NSInteger
    @NSManaged var screenNumber: NSInteger
    
    convenience init(context: NSManagedObjectContext,patientId: NSInteger,screenNumber: NSInteger) {
        let entity = NSEntityDescription.entity(forEntityName: "PatientScreenTracking",in: context)!
        self.init(entity: entity,insertInto: context)
        self.patientId = patientId
        self.screenNumber = screenNumber
    }
}
